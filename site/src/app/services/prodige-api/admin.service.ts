import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env/env.service';
import { JsonLdCollection } from '../../models/json-ld';
import { LayerType } from '../../models/layer-type.model';
import { map, Observable } from 'rxjs';
import { JSONLD_HEADER } from '../../models/jsonp-header';

@Injectable({
  providedIn: `root`,
})
export class AdminService {

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) {
  }

  getLayersType(): Observable<LayerType[]> {
    return this.httpClient.get<JsonLdCollection<LayerType>>( `${this.envService.urlAdmin}/api/lex_layer_types?page=1&itemsPage=10000`, {
      withCredentials: true,
      headers:         JSONLD_HEADER.headerGetCollection,
    }).pipe(
      map(( layerTypes ) => layerTypes[`hydra:member`] ? layerTypes[`hydra:member`] : []),
    );
  }
}
