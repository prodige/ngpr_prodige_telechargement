import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { LoggerService } from '../components/logger/logger.service';
import { Format } from "../models/format";
import { Projection } from "../models/projection";
import { Engine } from "../models/engine";
import { Territory } from "../models/territory";
import { JsonLdCollection } from '../models/json-ld';

@Injectable({
  providedIn: `root`,
})
export class UtilsService {
  private headerGet = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` };

  constructor( private httpClient: HttpClient, private envService: EnvService, private loggerService: LoggerService ) { }

  public getFormats( layerTypeId: number ): Observable<Format[]> {
    return this.httpClient.get<JsonLdCollection<Format>>(
      `${this.envService.urlAdmin}/api/formats?itemsPage=10000&lexLayerType.id=${layerTypeId}`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( formats ) => formats[`hydra:member`] ? formats[`hydra:member`] : []),
        catchError(( err ) => {
          this.loggerService.errorOnModal$( `Erreur de récupération des formats` );
          return throwError(() => <unknown>err );
        }),
      );
  }

  public getProjections(): Observable<Projection[]> {
    return this.httpClient.get<JsonLdCollection<Projection>>(
      `${this.envService.urlAdmin}/api/projections?itemsPage=1000`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( projections ) => {
          if ( !projections || !( projections[`hydra:member`])) {
            return null;
          }
          return projections[`hydra:member`];
        }),
        catchError(( err ) => {
          this.loggerService.errorOnModal$( `Erreur de récupération des formats` );
          return throwError(() => <unknown>err );
        }),
      );
  }

  public getEngines(): Observable<Engine[]> {
    return this.httpClient.get<JsonLdCollection<Engine>>(
      `${this.envService.urlAdmin}/api/engines?itemsPage=1000&lexEngineType.name=download`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( engines ) => {
          if ( !engines || !engines[`hydra:member`] || !( engines[`hydra:member`] instanceof  Array )) {
            return null;
          }
          return engines[`hydra:member`];
        }),
        catchError(( err ) => {
          this.loggerService.errorOnModal$( `Erreur de récupération des types de territoires` );
          return throwError(() => <unknown>err );
        }),
      );
  }

  public loadTerritories( type: string ):Observable<Territory[]>{

    return this.httpClient.get<Territory[]>(
      `${this.envService.urlAdmin}/api/engines/${type}/data`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( territories ) => {
          if ( !( territories instanceof Array )) {
            return null;
          }
          return territories;
        }),
        catchError(( err ) => {
          this.loggerService.errorOnModal$( `Erreur de récupération des territoires` );
          return throwError(() => <unknown>err );
        }),
      );
  }

}

