import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { map, Observable } from 'rxjs';
import { CasService } from './cas.service';
import { EnvService } from '../env/env.service';

@Injectable({
  providedIn: `root`,
})
export class CasGuard implements CanActivate {
  constructor(
    private casService: CasService,
    private envService: EnvService,
  ) {
  }

  canActivate(): Observable<boolean> {
    return this.casService.isConnected().pipe(
      map(( isConnected )  => true ));
  }

}
