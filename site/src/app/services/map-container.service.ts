import { Injectable } from '@angular/core';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { LoggerService } from '../components/logger/logger.service';
import { GetDataService } from './get-data.service';
import { switchMap, timer } from 'rxjs';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Geometry, MultiPolygon } from 'ol/geom';
import { Map } from 'ol';
import LayerGroup from 'ol/layer/Group';
import { Coordinate } from 'ol/coordinate';

@Injectable({
  providedIn: `root`,
})
export class MapContainerService {
  private drawLayer: VectorLayer<VectorSource> = null;

  private map: Map;

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
    private loggerService: LoggerService,
    private getDataService: GetDataService<FeatureCollection>,
  ) { }

  public getContext( url: string ){
    return timer( 250 ).pipe(
      switchMap(() =>  this.getDataService.getData$( url, null, null, true )),
    );
  }

  initDrawLayer( map: Map, rootLayer: LayerGroup ){
    const source = new VectorSource<Geometry>({ wrapX: false });
    this.drawLayer = new VectorLayer<VectorSource>({
      source,
    });

    rootLayer.getLayers().push( this.drawLayer );

    this.map = map;

    return this.drawLayer;
  }

  getCoordinate4326(): Coordinate[][][] {
    if ( !this.drawLayer?.getSource()?.getFeatures()?.length || !this.map ){
      return null;
    }

    const geom = this.drawLayer?.getSource()?.getFeatures()[0].getGeometry();
    const geom4326 = <MultiPolygon> geom.transform( this.map.getView().getProjection(), `EPSG:4326` );

    return geom4326.getCoordinates();
  }
}
