import { Injectable } from '@angular/core';
import {
  catchError,
  delay, finalize,
  map,
  Observable,
  of,
  repeat,
  ReplaySubject,
  Subject,
  switchMap,
  takeUntil, tap,
  throwError,
  timer,
} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { LoggerService } from '../components/logger/logger.service';
import { Download } from "../models/download";
import { Status, StatusFile, StatusFileApi, StatusType } from '../models/status';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';
import { strings } from '@angular-devkit/core';

@Injectable({
  providedIn: `root`,
})
export class DownloadService {
  private headerGet = { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` };

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
    private loggerService: LoggerService,
  ) { }

  public getDownloadInfo( downloadId: string ): Observable<Download> {
    return this.httpClient.get<Download>(
      `${this.envService.telechargementUrl}/api/info/${downloadId}`, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( download ) => {
          if ( !( download.layers instanceof Array )) {
            download.layers = [download.layers];
          }
          download.layersCount = download.layers.length;
          const layerName: string[] = [];
          const layerTable: string[] = [];
          download.layers.forEach(( layer ) => {
            layerName.push( layer.name );
            layerTable.push( layer.storagePath );
          });
          download.layersName = ( layerName.length > 0 ) ? layerName.join( `, ` ) : ``;
          download.layersTable = ( layerTable.length > 0 ) ? layerTable.join( `, ` ) : ``;

          return download;
        }),
      );
  }

  /**
   * demande de téléchargement
   * @param data
   */
  public postDownload( data: object ): Observable<Download> {
    console.log(data);
    return this.httpClient.post<Download>(
      `${this.envService.telechargementUrl}/api/downloads`, data, { withCredentials: true, headers: this.headerGet },
    )
      .pipe(
        map(( download ) => download ),
        catchError(( err ) => {
          this.loggerService.errorOnModal$( `Erreur lors de la création de la demande de téléchargement` );
          return throwError(() => <unknown>err );
        }),
      );
  }

  public getStateByUri( uri: string ){
    return this.httpClient.get<Status>( `${this.envService.telechargementUrl}${uri}` );
  }

  /**
   * demande l'état du fichier pendant un temps maximum de 30s
   */
  public askDownloadState( id: number ):Observable<StatusFile|unknown> {
    // Pour arrêter les demande une fois la génération finis ou en cas d'erreur
    const toStop = new Subject<boolean>();

    const status = <StatusFile>{ type: `IN_PROGRESS`, id: id };
    return of( true ).pipe(
      switchMap(() =>
        this.httpClient.get<StatusFileApi>( `${this.envService.telechargementUrl}/api/download/${id}/state`,
          { withCredentials: true, headers: this.headerGet }),
      ),
      map(( result ) => {
        if ( result?.status?.name ) {
          status.type = result.status.name;
        }

        if ( status.type !== `IN_PROGRESS` ) {
          toStop.next( true );
          toStop.complete();
        }

        return status;
      }),
      delay( 2000 ),
      repeat(),
      takeUntil( timer( 60000 )), // temps max de 30s
      takeUntil( toStop ),
      catchError(( error ): boolean | any => {
        console.error( error );
        this.loggerService.errorOnToast$( `Erreur pendant la génération du fichier` );
        toStop.complete();
        return false;
      }),
    );
  }
}

