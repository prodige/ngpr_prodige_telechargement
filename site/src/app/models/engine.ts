import { IDonwloadLayer } from './download';

export interface Engine {
  id: number;
  name: string;
  layer: IDonwloadLayer
}
