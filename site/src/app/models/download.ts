import { Coordinate } from 'ol/coordinate';

export type ExtractType = `list` | `polygon`;

export interface Download {
  id: number;
  uuid: string;
  metadataTitle: string;
  spatialRepresentationType: string;
  extractMap: string;
  layers: IDonwloadLayer[];
  layersCount: number;
  layersName: string;
  layersTable: string;
  status: string;
  resourceConstraints: ResourceConstraints
}
export interface ResourceConstraints {
  useLimitation: string[]
  otherConstraints: string
}


export interface FileParams{
  metadataUuid: string;
  userMail: string;
  direct: boolean;
  format: string;
  srs: string;
  extract?: IExtractList|IExtractPolygon;
}

export interface IExtractList{
  type: 'list';
  buffer: number;
  area_type_id: number;
  area_id: string[];
}

export interface IExtractPolygon{
  type: 'polygon';
  buffer: number;
  area: IExtractAera;
}

export interface IExtractAera{
  type: 'Polygon'
  coordinates: Coordinate[][][]
}

export interface IDonwloadLayer{
  storagePath: string,
  name: string,
  cguDisplay: boolean,
  cguMessage: string
}
