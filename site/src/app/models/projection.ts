export interface Projection {
  id: string;
  name: string;
  epsg: string;
}
