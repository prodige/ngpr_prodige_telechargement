export interface LayerType {
  id: number,
  name: string
}
