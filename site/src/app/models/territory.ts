export interface Territory {
  value: string;
  label: string;
}

