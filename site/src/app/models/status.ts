export type StatusType = 'FAILED' | 'SUCCESS' | 'IN_PROGRESS' | 'PLANNED' | 'LARGE_DATA';

export interface StatusFileApi{
  status:{
    name: StatusType
  }
}

export interface StatusFile{
  type:StatusType;
  id: number;
}


export interface Status {
  name: StatusType;
}
