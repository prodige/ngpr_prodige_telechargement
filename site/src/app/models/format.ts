export interface Format {
  id: string;
  name: string;
  code: string;
}
