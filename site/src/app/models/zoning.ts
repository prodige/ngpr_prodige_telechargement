export interface Zoning {
  id: string;
  name: string;
  code: string;
}

