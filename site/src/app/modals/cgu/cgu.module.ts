import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CGUComponent } from './cgu.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    CGUComponent,
  ],
  exports: [
    CGUComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
  ],
})
export class CguModuleModule { }
