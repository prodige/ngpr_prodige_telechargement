import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';
import { IDonwloadLayer } from '../../models/download';

@Component({
  selector:    `alk-modal-cgu`,
  templateUrl: `./cgu.component.html`,
  styleUrls:   [`./cgu.component.scss`],
})
export class CGUComponent {

  public active: number = 1;
  public layers: IDonwloadLayer[] = [];

  constructor( private activeModal: NgbActiveModal ) {}

  public closeModal() {
    this.activeModal.close();
  }

}
