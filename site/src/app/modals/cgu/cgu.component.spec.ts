import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CGUComponent } from './cgu.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';

describe('CGUComponent', () => {
  let fixture: ComponentFixture<CGUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CGUComponent ],
      imports: [NgbModule],
      providers: [ NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CGUComponent);

  });

  it('should create', () => {
    return true;
  });
});
