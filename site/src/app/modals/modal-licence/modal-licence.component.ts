import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {Download} from '../../models/download';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'alk-modal-licence',
  templateUrl: './modal-licence.component.html',
  styleUrls: ['./modal-licence.component.scss']
})
export class ModalLicenceComponent implements OnInit {
  @Input() download: Download;

  constructor(
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
  }
  close( modal: NgbActiveModal ){
    modal.close();
  }

  open( content: TemplateRef<unknown> ) {
    this.modalService.open( content, {
      backdrop: `static`,
      keyboard: false,
      size:     `lg`,
    });

  }
}
