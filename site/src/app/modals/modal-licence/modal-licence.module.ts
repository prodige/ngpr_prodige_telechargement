import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalLicenceComponent } from './modal-licence.component';
import {NgbNavModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    ModalLicenceComponent
  ],
  imports: [
    CommonModule,
    NgbNavModule
  ],
  exports: [
    ModalLicenceComponent
  ]
})
export class ModalLicenceModule { }
