import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalDownloadComponent } from './modal-download.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    ModalDownloadComponent,
  ],
  exports: [
    ModalDownloadComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
  ],
})
export class ModalDownloadModule { }
