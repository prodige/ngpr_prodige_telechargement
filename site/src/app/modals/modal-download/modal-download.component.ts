import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { DownloadService } from '../../services/download.service';
import { EnvService } from '../../services/env/env.service';
import { BaseComponent } from '../../components/base/base.component';
import { of, switchMap, takeUntil } from 'rxjs';
import { FileParams } from '../../models/download';
import { StatusFile } from '../../models/status';
import {CasService} from '../../services/cas/cas.service';

@Component({
  selector:    `alk-modal-download`,
  templateUrl: `./modal-download.component.html`,
  styleUrls:   [`./modal-download.component.scss`],
})
export class ModalDownloadComponent extends BaseComponent implements OnInit{

  public invalidForm: boolean = false;
  public direct: boolean = false;
  public pending: boolean = false;
  public fileUrl: string = null;

  public fileParams: FileParams = null;

  public status: StatusFile = { type: `IN_PROGRESS`, id: null };

  public askEmail: boolean = false;
  constructor(
    private activeModal: NgbActiveModal,
    private downloadService: DownloadService,
    private envService: EnvService,
    private casService: CasService
  ) {
    super();
  }

  ngOnInit(): void {
    const user = this.casService.getCasUser()
    if ( this.fileParams?.direct ){
      this.directDonwload();
    }
    else if(user && this.casService.getConnectedUser()) {
      this.askEmail = false;
      this.mailDonwload();
    } else{
      this.askEmail = true;
    }


  }

  initDownaload(){
    if(!this.fileParams.userMail){
      return;
    }
    this.askEmail = false;
    if ( this.fileParams?.direct ){
      this.directDonwload();
    }
    else {
      this.mailDonwload();
    }
  }

  public closeModal() {
    this.activeModal.close();
  }

  private mailDonwload(){
    this.downloadService.postDownload( this.fileParams ).pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( result ) => this.downloadService.getStateByUri( result.status )),
    )
      .subscribe({
        next: ( result ) => {
          this.status.type = result.name;
        },
        error: ( err ) => {
          this.status.type = `FAILED`;
          console.error( err );
        },
      });
  }

  private directDonwload(){
    this.downloadService.postDownload( this.fileParams ).pipe(
      switchMap(( result ) => {
        if ( result?.id ) {
          return this.downloadService.askDownloadState( result.id );
        } else {
          return of( null );
        }

      }),
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: ( result ) => {
          // Mise à jour de l'état en continue
          this.status = <StatusFile>result;
        },
        complete: () => {
          this.onComplete();
        },
        error: ( error ) =>{
          console.error( error );
          this.status.type = `FAILED`;
        },
      });
  }

  private onComplete(){
    // une fois la génération ou au bout de 30s
    console.log( `COMPLETE`, this.status );
    if ( this.status?.type === `SUCCESS` ) {
      this.fileUrl = `${this.envService.telechargementUrl}/api/download/${this.status.id}/file`;
    }
    if ( this.status?.type === `IN_PROGRESS` ) {
      this.status.type = `LARGE_DATA`;
    }
  }
}
