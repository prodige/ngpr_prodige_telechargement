import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDownloadComponent } from './modal-download.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';

describe('CGUComponent', () => {
  let fixture: ComponentFixture<ModalDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDownloadComponent ],
      imports: [NgbModule],
      providers: [ NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDownloadComponent);

  });

  it('should create', () => {
    return true;
  });
});
