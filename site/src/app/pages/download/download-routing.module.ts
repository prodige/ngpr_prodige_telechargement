import { RouterModule, Routes } from "@angular/router";
import { DownloadComponent } from "./download.component";
import { NgModule } from "@angular/core";

const routes: Routes = [
  {
    path:      ``,
    component: DownloadComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule],
})
export class DownloadRoutingModule {}
