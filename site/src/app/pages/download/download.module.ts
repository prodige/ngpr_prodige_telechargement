import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadComponent } from './download.component';
import { DownloadRoutingModule } from './download-routing.module';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { FormsModule } from "@angular/forms";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MapContainerModule } from '../../components/map-container/map-container.module';
import {ModalLicenceModule} from '../../modals/modal-licence/modal-licence.module';

@NgModule({
  declarations: [
    DownloadComponent,
  ],
  imports: [
    CommonModule,
    DownloadRoutingModule,
    NgMultiSelectDropDownModule,
    NgSelectModule,
    FormsModule,
    NgbModule,
    MapContainerModule,
    ModalLicenceModule,
  ],
})
export class DownloadModule {}
