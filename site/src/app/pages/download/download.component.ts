import { Component, OnInit } from '@angular/core';
import { Download, ExtractType, FileParams } from "../../models/download";
import { BaseComponent } from "../../components/base/base.component";
import { ActivatedRoute, ActivatedRouteSnapshot, Route } from "@angular/router";
import {Observable, of, switchMap, takeUntil} from "rxjs";
import { EnvService } from "../../services/env/env.service";
import { Format } from "../../models/format";
import { Projection } from "../../models/projection";
import { CGUComponent } from "../../modals/cgu/cgu.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Engine } from "../../models/engine";
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { UtilsService } from "../../services/utils.service";
import { NgForm } from "@angular/forms";
import { Territory } from "../../models/territory";
import { ModalDownloadComponent } from "../../modals/modal-download/modal-download.component";
import { DownloadService } from "../../services/download.service";
import { MapContainerService } from '../../services/map-container.service';
import { LayerType } from '../../models/layer-type.model';
import { HttpErrorResponse } from '@angular/common/http';
import { CasService } from '../../services/cas/cas.service';
import { LoggerService } from '../../components/logger/logger.service';


@Component({
  selector:    `alk-download`,
  templateUrl: `./download.component.html`,
  styleUrls:   [`./download.component.scss`],
})
export class DownloadComponent extends BaseComponent implements OnInit {

  public dropdownList: Territory[]  = [];
  public selectedItems: Territory[] = [];
  public dropdownSettings: IDropdownSettings = {};
  public download: Download = null;
  public formats: Format[] = [];
  public projections: Projection[] = [];
  public engines: Engine[] = [];
  public envData: EnvService = null;
  public selectorsLoading = true;
  public format: string;
  public projection: string;
  public tolerance = 100;
  public partialExtract = false;
  public selectExtractType: ExtractType = `list`;
  public territoryType: string;

  public displayCgu = true;

  public layerType: LayerType = null;
  public layerTypes: LayerType[] = [];

  constructor(
    private utilsService: UtilsService,
    private downloadService: DownloadService,
    private envService: EnvService,
    private ngbModal: NgbModal,
    private activatedRoute: ActivatedRoute,
    private mapContainerService: MapContainerService,
    private casService: CasService,
    private loggerService: LoggerService,
  ) {
    super();
    this.envData = envService;
  }

  ngOnInit(): void {

    this.dropdownSettings = {
      singleSelection:                false,
      idField:                        `value`,
      textField:                      `label`,
      selectAllText:                  $localize`:@@selectAll:Sélectionner tout`,
      unSelectAllText:                $localize`:@@unSelectAll:Tout désélectionner`,
      searchPlaceholderText:          $localize`:@@toResearch:Rechercher`,
      noDataAvailablePlaceholderText: $localize`:@@toResearch:Veuillez sélectionner un type de territoire`,
      allowSearchFilter:              true,
    };

    const downloadUuid =  this.activatedRoute.snapshot.paramMap.get( `uuid` );

    this.activatedRoute.data.pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(({ projections, engines, layerTypes }): Observable<Download> => {
        this.projections = <Projection[]> projections;
        this.engines = <Engine[]> engines;
        this.layerTypes = <LayerType[]> layerTypes;

        return this.downloadService.getDownloadInfo( downloadUuid );
      }),
      switchMap(( download: Download ) =>{
        this.download = download;
        const layerTypeFound = this.layerTypes.find(( layerType ) =>
          this.download.spatialRepresentationType === layerType.name,
        );

        this.displayCgu = !!this.download.layers.find(( layer ) => layer.cguDisplay );

        if ( !layerTypeFound ){
          return of( <Format[]>[]);
        }

        this.layerType = layerTypeFound;
        return this.utilsService.getFormats( layerTypeFound.id );
      }),
    )
      .subscribe({
        next: ( format ) => {
          this.formats = format;
        },
        error: ( err ) =>{
          console.error( err, err instanceof HttpErrorResponse );
          let httpError: HttpErrorResponse = null;
          if ( err instanceof HttpErrorResponse ){
            httpError = err;
          }

          if ( httpError?.status === 403 ){
            if ( this.casService.getConnectedUser()){
              window.location.href = `${this.envService.casUrl}/login?service=${encodeURIComponent( window.location.href )}`;
            } else {
              this.loggerService.errorOnModal$( `Accès au téléchargementrefusé` );
            }

          }
        },
      });

  }

  openCGU(): void {
    const modalRef = this.ngbModal.open( CGUComponent );
    ( <CGUComponent>modalRef.componentInstance ).layers = this.download.layers;
  }

  getTerritories( event: Event ): void {
    this.selectorsLoading = true;
    this.selectedItems = [];
    this.dropdownList = [];
    if (( event.target as HTMLInputElement ).value ) {
      const value: string = ( event.target as HTMLInputElement ).value;
      this.utilsService.loadTerritories( value )
        .pipe(
          takeUntil( this.endSubscriptions ),
        )
        .subscribe({
          next:  ( territories ) => this.dropdownList = territories,
          error: console.error,
        });
    }
    this.selectorsLoading = false;
  }

  submitForm( form: NgForm, downloadType: string ): void {

    const modalDownload = this.ngbModal.open( ModalDownloadComponent, {
      backdrop: `static`,
    });

    const modalRef = <ModalDownloadComponent>modalDownload.componentInstance;
    if ( !form.valid ) {
      modalRef.invalidForm = true;
      return;
    }
    const direct = ( downloadType === `downloadNow` );

    const fileParams = <FileParams>{
      metadataUuid: this.download.uuid,
      direct:       direct,
      format:       this.format,
      srs:          `EPSG:${this.projection}`,
    };

    if ( this.partialExtract ) {
      switch ( this.selectExtractType ) {
        case `polygon`:
          const coordinate4326 = this.mapContainerService.getCoordinate4326();
          if ( !coordinate4326?.length ){
            modalRef.invalidForm = true;
            return;
          }

          console.log( `Coordinate`, coordinate4326 );
          fileParams.extract = {
            type:         `polygon`,
            buffer:       this.tolerance,
            area:   {
              type:        `Polygon`,
              coordinates: coordinate4326,
            },
          };
          break;
        case `list`:
          fileParams.extract = {
            type:         `list`,
            buffer:       this.tolerance,
            area_type_id: parseInt( this.territoryType, 10 ),
            area_id:      this.selectedItems.map(( selected ) => `${selected.value}` ),
          };
          break;
      }

    }

    modalRef.fileParams = fileParams;
    modalRef.direct = direct;
    modalRef.pending = true;
    modalRef.fileUrl = null;
  }
}
