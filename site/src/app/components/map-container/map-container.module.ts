import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapContainerComponent } from './map-container.component';
import { MapModule } from '@alkante/visualiseur-core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MapContainerComponent,
  ],
  imports: [
    CommonModule,
    MapModule,
    NgbModule,
    FormsModule,
  ],
  exports: [
    MapContainerComponent,
  ],
})
export class MapContainerModule { }
