import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { switchMap, takeUntil } from 'rxjs';
import { MapIdService, MapService, VisualiseurCoreService } from '@alkante/visualiseur-core';
import { LoggerService } from '../logger/logger.service';
import { Map } from 'ol';

import { BaseComponent } from '../base/base.component';
import { MapContainerService } from '../../services/map-container.service';
import { Draw } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { createBox } from 'ol/interaction/Draw';
import { Download } from '../../models/download';
import { LayerType } from '../../models/layer-type.model';

type ExtractOption = `rect` | `polygon`;

@Component({
  selector:    `alk-map-container`,
  templateUrl: `./map-container.component.html`,
  styleUrls:   [`./map-container.component.scss`],
})
export class MapContainerComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input() downlaoadData: Download;
  @Input() layerType: LayerType;

  public canDisplayMap = false;

  public mapId = `visualiseur`;

  public map: Map;

  public drawSelect: ExtractOption = null;

  private draw: Draw = null;

  private drawLayer: VectorLayer<VectorSource> = null;

  constructor(
    private coreService: VisualiseurCoreService,
    private loggerService: LoggerService,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private mapContainerService: MapContainerService,
  ) {
    super();
  }

  ngOnInit(): void {

    // id de l'élément où sera affiché la carte
    this.mapService.setMapId( `visualiseur`, this.mapId || `main` );
    this.mapIdService.setId( this.mapId || `main` );

    // on récupère la carte openlayer
    this.mapService.getMapAfterComplete( this.mapId ).pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( map ) => {
        this.map = map;
        return this.coreService.getRootLayerGroup();
      }),
    )
      .subscribe(( rootLayer )=>{
        this.drawLayer = this.mapContainerService.initDrawLayer( this.map, rootLayer );
      });

    // on charge le context
    this.mapContainerService.getContext( this.downlaoadData.extractMap ).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: ( context ) => {
          if ( context ){
            this.coreService.setContext( context );
            this.canDisplayMap = true;
          }
        },
        error:    ( err ) => {
          console.error( err );
          this.loggerService.errorOnToast$( $localize`:@@contextNotLoad:Erreur pendant le chargement du context` );
        },
      });
  }

  onDrawSelect(){
    if ( this.draw ){
      this.drawLayer.getSource().clear();
      this.map.removeInteraction( this.draw );
    }

    switch ( this.drawSelect ){
      case `polygon`:
        this.draw = new Draw({
          source: this.drawLayer.getSource(),
          type:   `Polygon`,
        });
        break;
      case `rect`:
        this.draw = new Draw({
          source:           this.drawLayer.getSource(),
          type:             `Circle`,
          geometryFunction: createBox(),
        });
        break;
    }

    this.draw.on( `drawstart`, () => {
      this.drawLayer.getSource().clear();
    });

    this.draw.setActive( true );
    this.map.addInteraction( this.draw );
  }

  override ngOnDestroy() {
    super.ngOnDestroy();

    if ( this.draw ){
      this.drawLayer.getSource().clear();
      this.map.removeInteraction( this.draw );
      this.draw = null;
    }
  }

}
