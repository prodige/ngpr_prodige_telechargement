import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: `root`,
})
export class LoggerService {
  private msgOnModal$ = new ReplaySubject<string>( 1 );
  private msgOnToast$ = new ReplaySubject<string[]>( 1 );

  private toasts: string[] = [];

  errorOnModal$( message: string ): void{
    this.msgOnModal$.next( message );
  }


  getErrorOnModal$(): Observable<string>{
    return this.msgOnModal$.asObservable();
  }

  getErrorOnTaoast(): Observable<string[]>{
    return this.msgOnToast$.asObservable();
  }

  errorOnToast$( message: string ): void {
    this.toasts.push( message );
    this.msgOnToast$.next( this.toasts );
  }

  remove$( toast:string ): void {
    this.toasts = this.toasts.filter(( t ) => t !== toast );
    this.msgOnToast$.next( this.toasts );
  }

  clear$(): void {
    this.toasts.splice( 0, this.toasts.length );
    this.msgOnToast$.next( this.toasts );
  }

}
