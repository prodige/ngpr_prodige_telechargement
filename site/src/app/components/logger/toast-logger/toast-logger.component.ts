import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../logger.service';
import { BaseComponent } from '../../base/base.component';
import { takeUntil } from 'rxjs';

@Component({
  selector:    `alk-toast-logger`,
  templateUrl: `./toast-logger.component.html`,
  styleUrls:   [`./toast-logger.component.scss`],
})
export class ToastLoggerComponent extends BaseComponent implements OnInit {
  public toasts: string[] = [];

  constructor(
    private loggerService: LoggerService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loggerService.getErrorOnTaoast().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( toasts ) => { this.toasts = toasts; console.log( `NEW TOAST`, this.toasts ) });


  }

  remove( toast: string ): void {
    this.loggerService.remove$( toast );
  }
}
