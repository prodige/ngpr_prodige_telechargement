import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasGuard } from "./services/cas/cas.guard";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { DownloadResolver } from './resolvers/download.resolver';
import { ProjectionResolver } from './resolvers/projection.resolver';
import { EngineResolver } from './resolvers/engine.resolver';
import { LayerTypeResolver } from './resolvers/layer-type.resolver';


const routes: Routes = [
  {
    path:         `download/:uuid`,
    pathMatch:    `full`,
    canActivate:  [CasGuard],
    loadChildren: () => import( `./pages/download/download.module` ).then(({ DownloadModule }) => DownloadModule ),
    resolve:      {
      // download:    DownloadResolver,
      projections: ProjectionResolver,
      engines:     EngineResolver,
      layerTypes:   LayerTypeResolver,
    },
  },
  {
    path:       `**`,
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot( routes )],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
