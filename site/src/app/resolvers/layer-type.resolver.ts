import { Injectable } from '@angular/core';
import {
  Resolve,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AdminService } from '../services/prodige-api/admin.service';
import { LayerType } from '../models/layer-type.model';

@Injectable({
  providedIn: `root`,
})
export class LayerTypeResolver implements Resolve<LayerType[]> {
  constructor( private adminService: AdminService ) {
  }

  resolve(): Observable<LayerType[]> {
    return this.adminService.getLayersType();
  }
}
