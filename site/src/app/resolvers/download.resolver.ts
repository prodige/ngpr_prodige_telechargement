import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot, Router,
} from '@angular/router';
import { catchError, EMPTY, Observable } from 'rxjs';
import { Download } from "../models/download";
import { DownloadService } from "../services/download.service";

@Injectable({
  providedIn: `root`,
})
export class DownloadResolver implements Resolve<Download> {
  constructor( private downloadService: DownloadService, private router: Router ) {}

  resolve( route: ActivatedRouteSnapshot ): Observable<Download> {
    const downloadUuid = route.paramMap.get( `uuid` );
    return this.downloadService.getDownloadInfo( downloadUuid );
  }
}

