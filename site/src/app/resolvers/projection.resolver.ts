import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { catchError, EMPTY, Observable } from 'rxjs';
import { UtilsService } from "../services/utils.service";
import { Projection } from "../models/projection";

@Injectable({
  providedIn: `root`,
})
export class ProjectionResolver implements Resolve<Projection[]> {
  constructor( private utilsService: UtilsService, private router: Router ) {}

  resolve(): Observable<Projection[]> {
    return this.utilsService.getProjections()
      .pipe(
        catchError(() => {
          this.router.navigate([`/`])
            .catch( console.error );
          return EMPTY;
        }),
      );
  }
}

