import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { catchError, EMPTY, Observable } from 'rxjs';
import { UtilsService } from "../services/utils.service";
import { Engine } from "../models/engine";

@Injectable({
  providedIn: `root`,
})
export class EngineResolver implements Resolve<Engine[]> {
  constructor( private utilsService: UtilsService, private router: Router ) {}

  resolve(): Observable<Engine[]> {
    return this.utilsService.getEngines()
      .pipe(
        catchError(() => {
          this.router.navigate([`/`])
            .catch( console.error );
          return EMPTY;
        }),
      );
  }
}

