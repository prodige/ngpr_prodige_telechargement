#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
ROOTDIR="$SCRIPTDIR/../.."
COMMIT=`git rev-parse --short=8 HEAD`
DATE="`date '+%Y%m%dT%H%M'`"
. $SCRIPTDIR/env.sh

# Scan
echo "Scan docker web for vulnerabilities"
mkdir -p ${ROOTDIR}/trivy
docker run --name ${PROJECT_NAME}_trivy \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /tmp/cache-trivy:/root/.cache/ \
  -v ${ROOTDIR}/trivy:/tmp/report/ \
  aquasec/trivy:latest image \
  --timeout 5m \
  --format template --template "@contrib/html.tpl" \
  --output /tmp/report/cve_report.html \
  --severity CRITICAL,HIGH \
  ${PROJECT_NAME}_web:${COMMIT}

docker rm ${PROJECT_NAME}_trivy
