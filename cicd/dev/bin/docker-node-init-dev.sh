#!/bin/bash
set -e

SCRIPTDIR="$(cd $(dirname ${BASH_SOURCE[0]}/) && pwd)"
PROJECTDIR="$(cd ${SCRIPTDIR}/../../.. && pwd)"
PROJECTNAME="$(basename $PROJECTDIR)"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

# Si le docker tourne, il faut d'abord l'éteindre pour pouvoir supprimer le volume
if [ ! "$(docker container inspect -f '{{.State.Status}}' $NODE_CONTAINER_NAME)" == "running" ]; then
  echo -e "docker container $NODE_CONTAINER_NAME is not running"
  exit
fi

if [ ! -f "${PROJECTDIR}/site/angular.json" ]; then
  # Si le projet angular n'a pas déjà été initialisé
  # on le génère
  docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} bash -c "ng new ngpr_prodige_telechargement --strict --routing --prefix alk --package-manager npm --style scss --skip-git"

  # puis on remonte ce qui a été généré d'un niveau pour être juste dans site
  shopt -s dotglob nullglob
  mv $PROJECTDIR/site/${PROJECTNAME}/* $PROJECTDIR/site/
  rm -rf $PROJECTDIR/site/${PROJECTNAME}

  # On ajoute eslint
  docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} bash -c "npm i eslint @angular-eslint/builder @angular-eslint/eslint-plugin @angular-eslint/eslint-plugin-template @angular-eslint/schematics @angular-eslint/template-parser @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint-plugin-import eslint-plugin-jsdoc eslint-plugin-prefer-arrow"

  # Met à jour le fichier package.json pour mettre en place la ligne de lint
  oldTestScripts="\"test\": \"ng test\""
  newLintAndTestScripts="\"test\": \"ng test\",\n\t\t\"lint\"\: \"eslint \\\\\"\{src\,apps\,libs\,test\}\/\*\*\/\*\.ts\\\\\" \-\-fix\""
  sed -i "s/$oldTestScripts/$newLintAndTestScripts/g" "${PROJECTDIR}/site/package.json"

  # On fournit plusieurs fichiers commun au projets NestJs ( debug vscode avec launch.json, .gitignore, eslintrc.js, etc...)
  cp -R ../../init-dev/resources/. ${PROJECTDIR}/site/
  mv -f ${PROJECTDIR}/site/.vscode\.backup/ ${PROJECTDIR}/site/.vscode
  SITEDIRCREATING=false
  echo -e "** L'initialisation s'est terminée avec succès !";
else
  echo -e "Il semble que le projet ait déjà été initialisé"
fi
